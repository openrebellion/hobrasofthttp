CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

#
# NOTE: Order of subdirectory includes may be important.
#
ADD_SUBDIRECTORY(src) # Build the hobrasofthttpd library
#ADD_SUBDIRECTORY(example) # Build the example [NOT IMPLEMENTED]

