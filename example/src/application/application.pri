INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD

SOURCES += \
 $$PWD/application.cpp \
 $$PWD/list.cpp \
 $$PWD/item.cpp \

HEADERS += \
 $$PWD/application.h \
 $$PWD/list.h \
 $$PWD/item.h \
