INCLUDEPATH += $$PWD
DEPENDPATH  += $$PWD
INCLUDEPATH += ../../src

SOURCES += \
 $$PWD/abstractcontroller.cpp \
 $$PWD/controllerexample.cpp \
 $$PWD/requestmapper.cpp \
 $$PWD/json.cpp \
 $$PWD/httpd.cpp \

HEADERS += \
 $$PWD/abstractcontroller.h \
 $$PWD/controllerexample.h \
 $$PWD/requestmapper.h \
 $$PWD/json.h \
 $$PWD/httpd.h \

