#ifndef _Main_H_
#define _Main_H_

/**
 * @brief Namespace for Example
 * 
 * I do not like namespaces, but namespace is a great way to separate the documentation to two different groups:
 * Example and HobrasoftHttpd.
 */
namespace Example {
class Application;
extern Application *application;
}

#endif
